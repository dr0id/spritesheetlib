==============
spritesheetlib
==============

.. image:: https://badge.fury.io/py/spritesheetlib.png
    :target: http://badge.fury.io/py/spritesheetlib
    
.. image:: https://travis-ci.org/dr0id/spritesheetlib.png?branch=master
        :target: https://travis-ci.org/dr0id/spritesheetlib

.. image:: https://pypip.in/d/spritesheetlib/badge.png
        :target: https://crate.io/packages/spritesheetlib?version=latest


Spritesheetlib is a python package to load sprites from a spritesheet using a sprite sheet definition file in json format. Since the sprite sheet definition is in json, it could be loaded from other code (c#, js, ...) easily. It also includes a generator written in python.

* Free software: BSD license
* Documentation: http://spritesheetlib.rtfd.org.
* Discussion: http://dr0id.bitbucket.org/python/spritesheetlib/


Features
--------

* sprite sheet definition in json
* supports sprites in grid arrangement and non grid arrangement
* support for polygon shaped sprites
* properties (ever wanted to store additional values for a sprite?)
* grouping by property
* some more


Examples
--------

The spritesheetlib is used to load sprite definition files.

.. sourcecode:: python

        # import
        from spritesheetlib import SpritesheetLib10, DummyLogger, FileInfo

        # make an instance
        spritesheetlib = SpritesheetLib10(DummyLogger())

        # load the sprites
        sprites = spritesheetlib.load_spritesheet(FileInfo("data/anim.png"))

        # use the sprites
        current_sprite = sprites[index]


Now the sprites are loaded in a SpritesList instance. Note that the sprite class used is just a container and is
defined in the spritesheetlib module as well. It has only a few attributes (image, anchor, gid, properties). This
can be used, but is not that useful because you would have
to know which sprite is which to put the animations together correctly. Therefore this is not a simple list, but
a SpritesList instance. It has some little extras. To make that work, each sprite need some properties. If the sprite
sheet definition was generated with the spritesheet_mask_generator script, then each sprite has a 'row' and
'col' property set. Using these one can get the rows or columns easily.

.. sourcecode:: python

        # get the rows, ordered ascending
        rows = sprites.get_rows()

        # get the columns, ordered ascending
        cols = sprites.get_columns()

        # each row may represent a different direction like up, down, left, right
        # e.g. running up is in first row
        current_sprite = rows[0][index]


You may wonder how the order of the sprites is correct. Each sprite has a 'GID' assigned to it (through the sprite
sheet definition). When the sprites are loaded they are loaded in the same order as defined in the sprite sheet
definition. This is normally sorted according the GID ascending. Since it is a list, it can be sorted in different
order easily if desired. This alone does not ensure correct order, but most sprite sheets go either from left to right
or from top to bottom. In both cases the GID values increase.

In bigger spritesheet there may be more than one animation on it. It may contain multiple actions and for each action
the sprites for all directions. Therefore there is another useful method, but only works if the 'action' and 'facing'
properties are set correctly for each sprite. If done so then you can get a dictionary.

.. sourcecode:: python

        # get the dictionary
        sprite_sheet_dict = sprites.get_grouped_by_action_and_facing()

        # use it
        action = "walk"
        facing = "up"
        current_sprite = sprite_sheet_dict[action][facing][index]


More tutorials can be found in the `Wiki <https://bitbucket.org/dr0id/spritesheetlib/wiki>`_.