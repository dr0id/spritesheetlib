# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of HG_pytmxloader
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# * Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""

import json

from pytmxloader.spritesheetlib import SpritesheetLib10, AABB


ELEMENT_SPRITES = SpritesheetLib10.ELEMENTS.SPRITES
ELEMENT_POINTS = SpritesheetLib10.ELEMENTS.POINTS

__version__ = '1.0.3.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2014"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module 

import pygame

from test_spritesheetlib import TestData, LoggerMock
from pytmxloader.spritesheetlib import move_points


def main():
    """
    The main method.

    """
    screen_size = 800, 600

    pygame.init()

    screen = pygame.display.set_mode(screen_size, 0, 32)

    # (TestData.SDEF_GRID_2X2, TestData.SDEF_GRID_2X2)
    # (TestData.SDEF_OVERLAPPING_SPRITES, TestData.SDEF_OVERLAPPING_SPRITES, None, 0),
    # (TestData.SDEF_OVERLAPPING_SPACING0, TestData.SDEF_OVERLAPPING_SPRITES, 0, 0),
    # (TestData.SDEF_OVERLAPPING_SPACING3, TestData.SDEF_OVERLAPPING_SPRITES, 3, 0),
    # (TestData.SDEF_EXTREME_OVERLAPPING_SPACING5, TestData.SDEF_EXTREME_OVERLAPPING, 5, 0),
    # (TestData.SDEF_EXTREME_OVERLAPPING, TestData.SDEF_EXTREME_OVERLAPPING, None, 0),
    # (TestData.SDEF_EXTREME_OVERLAPPING_MARGIN10, TestData.SDEF_EXTREME_OVERLAPPING, None, 10),

    data = [
        (TestData.SDEF_GRID_2X2, TestData.SDEF_GRID_2X2, None, 0),
        (TestData.SDEF_GRID_2X2, TestData.SDEF_GRID_2X2, 0, 0),
        (TestData.SDEF_GRID_2X2, TestData.SDEF_GRID_2X2, 1, 0),
        (TestData.SDEF_OVERLAPPING_SPRITES, TestData.SDEF_OVERLAPPING_SPRITES, None, 0),
        (TestData.SDEF_OVERLAPPING_SPACING0, TestData.SDEF_OVERLAPPING_SPRITES, 0, 0),
        (TestData.SDEF_OVERLAPPING_SPACING3, TestData.SDEF_OVERLAPPING_SPRITES, 3, 0),
        (TestData.SDEF_EXTREME_OVERLAPPING_SPACING5, TestData.SDEF_EXTREME_OVERLAPPING, 5, 0),
        (TestData.SDEF_EXTREME_OVERLAPPING, TestData.SDEF_EXTREME_OVERLAPPING, None, 0),
        (TestData.SDEF_EXTREME_OVERLAPPING_MARGIN10, TestData.SDEF_EXTREME_OVERLAPPING, None, 10),
    ]
    logger_mock = LoggerMock()
    spritesheet_lib = SpritesheetLib10(logger_mock)

    idx = 0
    running = True
    while running:
        for event in [pygame.event.wait()]:
            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    running = False
                elif event.key == pygame.K_RIGHT:
                    idx += 1
                    idx %= len(data)
                elif event.key == pygame.K_LEFT:
                    idx -= 1
                    idx %= len(data)

        expected, in_sdef, spacing, margin = data[idx]
        in_sdef = json.loads(json.dumps(in_sdef))  # make a copy

        actual_sprite_definition = spritesheet_lib.adjust_spacing(in_sdef, spacing)
        actual_sprite_definition = spritesheet_lib.adjust_margin(actual_sprite_definition, margin)

        colors = [(255, 255, 255, 128), (255, 0, 0, 128), (255, 0, 255, 128)]
        offsets = [(100, 100), (300, 100), (600, 300)]

        for sdef_idx, sprite_definition in enumerate([in_sdef, actual_sprite_definition, expected]):
            color = colors[sdef_idx]
            off_x, off_y = offsets[sdef_idx]

            for sprite in sprite_definition[ELEMENT_SPRITES]:
                points = sprite[ELEMENT_POINTS]
                points = move_points(points, off_x, off_y)

                aabb = AABB.from_points(points)

                pygame.draw.rect(screen, (50, 50, 50, 128), aabb.to_rect_tuple())

                # pygame.draw.polygon(surf, (255, 255, 255), points, 0)
                pygame.draw.lines(screen, color, True, points, 1)

        pygame.display.flip()
        screen.fill((0, 0, 0))
        pygame.display.set_caption("idx: {0} spacing: {1} margin: {2}".format(idx, spacing, margin))


if __name__ == '__main__':
    main()
