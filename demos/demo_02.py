"""demo_02.py - dual-purpose script: 1) generate sdef; 2) display the spritesheet

usage: demo_02.py [-h] [-g]

Spritesheet mask generator and demo for othelia spritesheet. Run without args
to play demo.

optional arguments:
  -h, --help      show this help message and exit
  -g, --generate  generate othelia.png.sdef and exit
"""

import argparse
import json
import re
import sys
sys.path.insert(0, '..')

import pygame
from pygame.locals import *

from spritesheetlib import SpritesheetLib10, DummyLogger, FileInfo
from spritesheetlib import spritesheetmaskgeneratorlib as ssm_gen


image_file = 'image/othelia_256x256.png'


def parse_args():
    parser = argparse.ArgumentParser(
        description='Spritesheet mask generator and demo for othelia spritesheet. Run without args to play demo.')
    parser.add_argument('-g', '--generate', dest='generate', action='store_true',
                        help='generate othelia.png.sdef and exit')
    return parser.parse_args()


def generate_sdef():

    # The spritesheet has the following image arrangement:
    #     row1: walk      x  31 frames
    #     row2: run       x  31 frames
    #     row3: jumpup    x  1 frame
    #     row4: jumpdown  x  1 frame
    #     row5: duck      x  1 frame

    # create the properties to give to the generator
    properties_31x5 = """{
    '0:31': {'pose': 'walk'},
    '31:62': {'pose': 'run'},
    '62:63': {'pose': 'jumpup'},
    '93:94': {'pose': 'jumpdown'},
    '124:125': {'pose': 'duck'},
    'mode': 'update'
    }"""
    props = properties_31x5
    props = props.replace(" ", "")  # this is ESSENTIAL, otherwise the commandline does not work!
    props = ''.join(props.splitlines())
    # props = props.replace("'", '"')  # this is already done by the lib

    # see if it blows up; easier to debug here
    # print by 10's so we can count the offset in the json message
    # print('Props 10 per line:')
    # for i in range(0, len(props), 10):
    #     print('{:>2d}  {}'.format(i // 10, props[i:i + 10]))
    # s = json.loads(props)

    # call main, which understands the command line syntax
    command = re.sub('\s+', ' ', """ --properties={props}
        --force
        create_grid
        --tile_width=256
        --tile_height=256
        31 5
        {file}.sdef
        """.format(props=props, file=image_file))
    ssm_gen.SpriteSheetMaskGenerator(SpritesheetLib10()).main(command.split())


def main():
    pygame.init()
    screen = pygame.display.set_mode((256, 256))
    clock = pygame.time.Clock()

    # Load the sprites.
    spritesheetlib = SpritesheetLib10(DummyLogger())
    file_info = FileInfo(image_file + '.sdef')
    sprites = spritesheetlib.load_spritesheet(file_info)

    # Get sprites by property.
    sprites_grouped = sprites.get_grouped_by_property('pose')
    walk_sprites = sprites_grouped['walk']
    run_sprites = sprites_grouped['run']
    jumpup_sprites = sprites_grouped['jumpup']
    jumpdown_sprites = sprites_grouped['jumpdown']
    duck_sprites = sprites_grouped['duck']

    # This is a container we can easily cycle through.
    sprite_seq = [walk_sprites, run_sprites, jumpup_sprites, jumpdown_sprites, duck_sprites]

    # Runtime variables.
    repeat = 0
    i = 0  # index to which_sprites
    j = 0  # index to sprite_seq
    max_fps = 1000.0 // len(walk_sprites)
    which_sprites = walk_sprites

    running = True
    while running:
        clock.tick(max_fps)
        for e in pygame.event.get():
            if e.type == QUIT:
                running = False

        # Increment the index with wrap.
        i = (i + 1) % len(which_sprites)

        # Advance the repetitions on 0.
        if i == 0:
            if which_sprites in (walk_sprites, run_sprites):
                # repeat 31-frame sequence four times
                repeat = (repeat + 1) % 4
            else:
                # repeat 1-frame sequence 31 times (lapse one second)
                repeat = (repeat + 1) % max_fps
            # Advance the sprite sequence on 0.
            if repeat == 0:
                j = (j + 1) % len(sprite_seq)
                which_sprites = sprite_seq[j]

        sprite = which_sprites[i]

        screen.fill((0, 0, 0))
        screen.blit(sprite.image, (0, 0))
        pygame.display.flip()


if __name__ == '__main__':
    args = parse_args()
    if args.generate:
        generate_sdef()
        sys.exit()
    else:
        main()
