"""demo_01.py - Using the generator with a simple sprite sequence

Given:
    - a spritesheet of an animated running man
    - 30 frames, left to right in a single row
    - each frame is 256x256 pixels

Instructions:

1. Generate the spritesheet definition file. It is a grid of 30 frames.

    $ cd image
    $ ls -l hero.png
    $ python ../../spritesheetlib/spritesheet_mask_generator.py create_grid \
    --tile_width 256 --tile_height 256 31 1 hero.png.sdef -f

2. Use the spritesheet library to load the sprite sheet.

3. Use the spritesheet library to produce a sprite list.
"""


import sys
sys.path.insert(0, '../')  # find the lib is only the repository has been cloned

import pygame

from spritesheetlib import SpritesheetLib10, DummyLogger, FileInfo


spritesheetlib = SpritesheetLib10(DummyLogger())

pygame.init()
screen = pygame.display.set_mode((256, 256))
screen_rect = screen.get_rect()

filename = 'image/hero.png.sdef'
file_info = FileInfo(filename)
sprites = spritesheetlib.load_spritesheet(file_info)
frame_rate = 1000 // len(sprites)

running = True
while running:
    pygame.time.wait(frame_rate)
    for e in pygame.event.get():
        if e.type == pygame.QUIT:
            running = False
        elif e.type == pygame.KEYDOWN:
            if e.key == pygame.K_ESCAPE:
                running = False

    screen.fill((0, 0, 0))
    sprite = sprites.pop(0)
    screen.blit(sprites[0].image, (0, 0))
    sprites.append(sprite)
    pygame.display.flip()

pygame.quit()
