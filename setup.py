#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
# del os.link  # as suggested here to avoid some hardlinking problems: http://stackoverflow.com/questions/17223047/why-is-my-python-setup-script-failing
import sys


try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

if sys.argv[-1] == 'publish':
    os.system('python setup.py sdist upload')
    sys.exit()

readme = open('README.rst').read()
history = open('HISTORY.rst').read().replace('.. :changelog:', '')

setup(
    name='spritesheetlib',
    version='1.0.3.0',
    description='Spritesheetlib is a python package to load sprites from a spritesheet using a sprite sheet definition file in json format. It also includes a generator.',
    long_description=readme + '\n\n' + history,
    author='DR0ID',
    author_email='dr0iddr0id [at] googlemail [dot] com',
    url='https://bitbucket.org/dr0id/spritesheetlib',
    packages=[
        'spritesheetlib',
    ],
    package_dir={'spritesheetlib': 'spritesheetlib'},
    include_package_data=True,
    install_requires=[
    ],
    license="BSD",
    zip_safe=False,
    keywords='spritesheetlib',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Natural Language :: English',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
    ],
    test_suite='tests',
)
