How to generate the docs
========================

Prerequisites
-------------

hg (mercurial DVCS)
python 2.7 or > 3.2
sphinx  (best to pip install -U sphinx



Getting the sources
-------------------

Clone the repository::

    hg clone https://bitbucket.org/dr0id/spritesheetlib spritesheetlib

If you want to have the wiki too in the documentation then you need to clone the wiki too::

    hg clone https://bitbucket.org/dr0id/spritesheetlib/wiki spritesheetlib/docs/wiki

.. note:: The destination path is within the spritesheetlib directory structure.



Generating the sources
----------------------

Open a command shell and go to spritesheetlib/docs.
On windows following should suffice::

    make html

On other platforms the command is probably the same (follow sphinx instructions how to generate it).
The documentation is now available in::

    spritesheetlib/docs/_build/html

Open *index.html* with your favorite browser.

