.. complexity documentation master file, created by
   sphinx-quickstart on Tue Jul  9 22:26:36 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to spritesheetlib's documentation!
==========================================

Contents:

.. toctree::
   :maxdepth: 4

   readme
   installation
   usage
   wiki_pages_index
   how_to_generate_docs
   contributing
   authors
   history
   modules_index


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
